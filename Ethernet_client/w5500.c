#include "w5500.h"
#include "spi.h"
#include "registers.h"
#include <util/delay.h>
#include "serial.h"

void w5500_init(short ss_pin) //ss = slave select, pin 32 sur le w5500
{
	_delay_ms(1000);

	DDRB |= 0x04; // configure la pin SS en sortie (10 de l arduino et 16 de l atmega)
	PORTB |= 0x04;
	spi_init();
	
	short mr = w5500_read(MR_address, 0x00); //lire le registre MR
	w5500_write(MR_address, 0x04, mr | 0x80); //reset des registres

	short i;
	for(i=0; i<MAX_SOCK_NUM; i++)
	{
		/*uint8_t ctrl_byte = 0x0C + (i<<5);
		w5500_write( 0x1E, ctrl_byte, 2);
		w5500_write( 0x1F, ctrl_byte, 2);*/

		w5500_writeSN8(i, 0x1E, 2); //met le buffer de reception a 2kB
		w5500_writeSN8(i, 0x1F, 2); //met le buffer de transmission a 2kB
	}
}

void w5500_write(int address, short control, short data)
{
	PORTB &= 0xFB; //met la pin SS a 0

	spi_fast_shift(address >> 8);
	spi_fast_shift(address & 0xFF);
	spi_fast_shift(control);
	spi_fast_shift(data);

	PORTB |= 0x04; //met la pin SS a 1
}

void w5500_write_string(int address, short control, char* data, int len)
{
	PORTB &= 0xFB; //met la pin SS a 0

	spi_fast_shift(address >> 8);
	spi_fast_shift(address & 0xFF);
	spi_fast_shift(control);
	int i;
	for(i=0;i<len;i++)
	{
		spi_fast_shift(data[i]);
	}

	PORTB |= 0x04; //met la pin SS a 1
}

short w5500_read(int address, short control)
{
	//address += 1;
	PORTB &= 0xFB; //met la pin SS a 0
	spi_fast_shift(address >> 8);
	spi_fast_shift(address & 0xFF);
	spi_fast_shift(control);
	short data = spi_fast_shift(0);
	PORTB |= 0x04; //met la pin SS a 1

	return data;
}

void w5500_read_str(int address, short control, short* buffer, short len)
{
	//address += 1;
	PORTB &= 0xFB; //met la pin SS a 0
	spi_fast_shift(address >> 8);
	spi_fast_shift(address & 0xFF);
	spi_fast_shift(control);
	int i = 0;
	for(i=0;i<len;i++)
	{
		buffer[i] = spi_fast_shift(0);
	}
	PORTB |= 0x04; //met la pin SS a 1
}

void w5500_writeSN8(short sock, int address, short data)
{
	short ctrl_byte = (sock<<5)+0x0C;
    w5500_write(address, ctrl_byte, data);
}

void w5500_writeSN16(short sock, int address, int data)
{
	if(sock == 0 && address == 0x0024)
		serial_write('d');
	
	short ctrl_byte = (sock<<5)+0x0C;
    w5500_write(address, ctrl_byte, data >> 8);
	w5500_write(address+1, ctrl_byte, data & 0xFF);
}

short w5500_readSN8(short sock, int address)
{
	short ctrl_byte = (sock<<5)+0x08;
	return w5500_read(address, ctrl_byte); //renvoie les data correspondantes
}

void w5500_cmd_sock(short sock, short command)
{
	w5500_writeSN8(sock, 0x0001, command);
}
