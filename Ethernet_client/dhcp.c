#include <stdlib.h>
#include <time.h>
#include <string.h> //some memory can maybe be saved not using this library
#include "dhcp.h"
#include "udp.h"
#include "serial.h"

#define DHCP_SERVER_PORT 67 //from server to client

#define DHCP_HTYPE10MB 1
#define DHCP_HLEN10MB 6
#define DHCP_HOPS 0
#define DHCP_FLAGSBROADCAST	0x8000
#define CIADDR 0
#define YIADDR 0
#define SIADDR 0
#define GIADDR 0
#define MAGIC_COOKIE 0x63825363

/* DHCP state machine. */
#define STATE_DHCP_START 0
#define	STATE_DHCP_DISCOVER 1
#define	STATE_DHCP_REQUEST 2
#define	STATE_DHCP_LEASED 3
#define	STATE_DHCP_REREQUEST 4
#define	STATE_DHCP_RELEASE 5

/* DHCP OP code*/
#define DHCP_BOOTREQUEST 1
#define DHCP_BOOTREPLY 2

/* DHCP message type */
#define	DHCP_DISCOVER 1
#define DHCP_OFFER 2
#define	DHCP_REQUEST 3
#define	DHCP_DECLINE 4
#define	DHCP_ACK 5
#define DHCP_NAK 6
#define	DHCP_RELEASE 7
#define DHCP_INFORM 8

/* OPTION CODES*/
#define PAD_OPTION 0
#define SUBNET_MASK 1
#define TIMER_OFFSET 2
#define ROUTERS_ON_SUBNET 3
#define DNS 6
#define HOST_NAME 12
#define DOMAIN_NAME 15
#define DHCP_REQUESTED_IP_ADDR 50
#define DHCP_IP_ADDR_LEASE_TIME 51
#define DHCP_MESSAGE_TYPE 53
#define DHCP_SERVER_IDENTIFIER 54
#define DHCP_PARAM_REQUEST 55
#define DHCP_T1_VALUE 58
#define DHCP_T2_VALUE 59
#define DHCP_CLIENT_IDENTIFIER 61
#define END_OPTION 255

struct Ip
{
	short subnet_mask[4];
	short gateway[4];
	short local_address[4];
	short dhcp_server_address[4];
	short dns_server_address[4];
};

//structure to store received fields in dhcp response, only used fields are put in order to save memory
struct Dhcp_fields
{
	short op;
	short xid[4];
	short yiaddr[4];
	short chaddr[6];
};

int dhcp_begin(short* mac_address, unsigned long timeout, unsigned long respose_timeout)
{
	
}

void dhcp_request_lease(short* mac_address)
{
	serial_write('d');
	serial_write('h');
	serial_write('c');
	serial_write('p');
	serial_write('\n');
	
	IP ip;
	//ip.local_address = {192, 168, 1, 177};
	
	//create a random unsigned long
	unsigned long dhcp_transaction_id = rand();
	dhcp_transaction_id = dhcp_transaction_id << 16 || rand();
	
	time_t* start_time = NULL;
	time_t* send_time = NULL;
	*start_time = time(start_time);
	
	short dhcp_state = STATE_DHCP_START;
	
	while(dhcp_state != STATE_DHCP_LEASED)
	{
		if(dhcp_state == STATE_DHCP_START)
		{
			dhcp_transaction_id++;
			dhcp_send_request(dhcp_transaction_id, DHCP_DISCOVER, (int)(time(send_time) - *start_time), &ip);
			dhcp_state = STATE_DHCP_DISCOVER;
		}
		else if(dhcp_state == STATE_DHCP_REREQUEST)
		{
			dhcp_transaction_id++;
			dhcp_send_request(dhcp_transaction_id, DHCP_REQUEST, (int)(time(send_time) - *start_time), &ip);
			dhcp_state = STATE_DHCP_REQUEST;
		}
		else if(dhcp_state == STATE_DHCP_DISCOVER)
		{
			
		}
		else if(dhcp_state == STATE_DHCP_REQUEST)
		{
			
		}
	}
			
	
	dhcp_send_request(dhcp_transaction_id, DHCP_REQUEST, (int)(time(send_time) - *start_time), &ip);
}

void dhcp_send_request(unsigned long dhcp_transaction_id, short message_type, int seconds_elapsed, IP* p_ip)
{
	//short buffer[32] = {0};
	
	const char host_name[6] = "WIZnet";
	
	int offset = 0;
	int* p_offset = &offset;
	
	short dst_addr[4] = {255,255,255,255}; //limited broadcast address
	
	serial_write('a');
	udp_init(dst_addr, DHCP_SERVER_PORT, DHCP_SERVER_PORT, 2);
	
	udp_write_byte(DHCP_BOOTREQUEST, 2, p_offset); //op
	udp_write_byte(DHCP_HTYPE10MB, 2, p_offset); //htype for 10Mb ethernet
	udp_write_byte(DHCP_HLEN10MB, 2, p_offset); //hlen
	udp_write_byte(DHCP_HOPS, 2, p_offset); //hops
	
	//xid
	udp_write_byte((short)(dhcp_transaction_id >> 24), 2, p_offset);
	udp_write_byte((short)(dhcp_transaction_id >> 16), 2, p_offset);
	udp_write_byte((short)(dhcp_transaction_id >> 16), 2, p_offset);
	udp_write_byte((short)dhcp_transaction_id, 2, p_offset);
	
	//secs
	udp_write_byte((short)(seconds_elapsed >> 8), 2, p_offset);
	udp_write_byte((short)seconds_elapsed, 2, p_offset);
	serial_write('b');
	//flags
	udp_write_byte((short)(DHCP_FLAGSBROADCAST >> 8), 2, p_offset);
	udp_write_byte((short)DHCP_FLAGSBROADCAST, 2, p_offset);
	
	udp_write_byte(CIADDR, 2, p_offset); //ciaddr
	udp_write_byte(YIADDR, 2, p_offset); //yiaddr
	udp_write_byte(SIADDR, 2, p_offset); //siaddr
	udp_write_byte(GIADDR, 2, p_offset); //giaddr
	
	//chaddr (mac address of client)
	udp_write_byte(0xDE, 2, p_offset);
	udp_write_byte(0xAD, 2, p_offset);
	udp_write_byte(0xBE, 2, p_offset);
	udp_write_byte(0xEF, 2, p_offset);
	udp_write_byte(0xFE, 2, p_offset);
	udp_write_byte(0xED, 2, p_offset);
	
	//complete chaddr with some 0
	for(short i = 0; i<10; i++)
		udp_write_byte(0x00, 2, p_offset);
	
	//0 in sname and file (192 octets)
	for(short i = 0; i<192; i++)
		udp_write_byte(0x00, 2, p_offset);
	
	/* OPTIONS*/
	//magic cookie
	udp_write_byte((short)(MAGIC_COOKIE >> 24), 2, p_offset);
	udp_write_byte((short)(MAGIC_COOKIE >> 16), 2, p_offset);
	udp_write_byte((short)(MAGIC_COOKIE >> 8), 2, p_offset);
	udp_write_byte((short)MAGIC_COOKIE, 2, p_offset);
	
	//message type
	udp_write_byte(DHCP_MESSAGE_TYPE, 2, p_offset);
	udp_write_byte(0x01, 2, p_offset);
	udp_write_byte(message_type, 2, p_offset);
	
	//client identifier
	udp_write_byte(DHCP_CLIENT_IDENTIFIER, 2, p_offset);
	udp_write_byte(0x07, 2, p_offset);
	udp_write_byte(0x01, 2, p_offset);
	udp_write_byte(0xDE, 2, p_offset);
	udp_write_byte(0xAD, 2, p_offset);
	udp_write_byte(0xBE, 2, p_offset);
	udp_write_byte(0xEF, 2, p_offset);
	udp_write_byte(0xFE, 2, p_offset);
	udp_write_byte(0xED, 2, p_offset);
	serial_write('c');
	//host name
	udp_write_byte(HOST_NAME, 2, p_offset);
	udp_write_byte(strlen(host_name) + 6, 2, p_offset);
	udp_write_byte('W', 2, p_offset);
	udp_write_byte('I', 2, p_offset);
	udp_write_byte('Z', 2, p_offset);
	udp_write_byte('n', 2, p_offset);
	udp_write_byte('e', 2, p_offset);
	udp_write_byte('t', 2, p_offset);
	char buf[2];
	dhcp_hex_to_ascii(buf, 0xEF);
	dhcp_hex_to_ascii(buf, 0xFE);
	dhcp_hex_to_ascii(buf, 0xED);
	
	if(message_type == DHCP_REQUEST)
	{
		/*This option is used in a client request to allow the
		*client to request that a particular IP address be assigned.*/
		udp_write_byte(DHCP_REQUESTED_IP_ADDR, 2, p_offset); //code
		udp_write_byte(0x04, 2, p_offset); //length
		udp_write_byte(p_ip->local_address[0], 2, p_offset);
		udp_write_byte(p_ip->local_address[1], 2, p_offset);
		udp_write_byte(p_ip->local_address[2], 2, p_offset);
		udp_write_byte(p_ip->local_address[3], 2, p_offset);
		
		/*DHCP clients indicate
		 * which of several lease offers is being accepted by including this
		 * option in a DHCPREQUEST message.*/
		udp_write_byte(DHCP_SERVER_IDENTIFIER, 2, p_offset); //code
		udp_write_byte(0x04, 2, p_offset); //length
		udp_write_byte(p_ip->dhcp_server_address[0], 2, p_offset);
		udp_write_byte(p_ip->dhcp_server_address[1], 2, p_offset);
		udp_write_byte(p_ip->dhcp_server_address[2], 2, p_offset);
		udp_write_byte(p_ip->dhcp_server_address[3], 2, p_offset);
	}
	
	udp_write_byte(DHCP_PARAM_REQUEST, 2, p_offset); //code
	udp_write_byte(0x06, 2, p_offset); //length
	udp_write_byte(SUBNET_MASK, 2, p_offset);
	udp_write_byte(ROUTERS_ON_SUBNET, 2, p_offset);
	udp_write_byte(DNS, 2, p_offset);
	udp_write_byte(DOMAIN_NAME, 2, p_offset);
	udp_write_byte(DHCP_T1_VALUE, 2, p_offset);
	udp_write_byte(DHCP_T2_VALUE, 2, p_offset);
	
	udp_write_byte(END_OPTION, 2, p_offset); //end of options
	serial_write('d');
	udp_send(2); //send the request
	serial_write('e');
}

void dhcp_read_response(IP* p_ip)
{
	short type;
	short opt_len;
	
	int decal = 0;
	int* p_decal = &decal;
	
	short* src_ip = NULL;
	int* src_port = NULL;
	udp_parse(2, src_ip, src_port, p_decal);
	
	DHCP_FIELDS dhcp_fields;
	
	short* poubelle = NULL;
	
	udp_read(2, p_decal, &(dhcp_fields.op), 1);
	udp_read(2, p_decal, poubelle, 1); //don't keep htype
	udp_read(2, p_decal, poubelle, 1); //don't keep hlen
	udp_read(2, p_decal, poubelle, 1); //don't keep hops
	udp_read(2, p_decal, dhcp_fields.xid, 4);
	udp_read(2, p_decal, poubelle, 2); //don't keep secs
	udp_read(2, p_decal, poubelle, 2); //don't keep flags
	udp_read(2, p_decal, poubelle, 4); //don't keep ciaddr
	udp_read(2, p_decal, dhcp_fields.yiaddr, 4);
	udp_read(2, p_decal, poubelle, 4); //don't keep siaddr
	udp_read(2, p_decal, poubelle, 4); //don't keep giaddr
	udp_read(2, p_decal, dhcp_fields.chaddr, 6);
	udp_read(2, p_decal, poubelle, 10); //don't keep the rest of chaddr
	
	if(dhcp_fields.op == DHCP_BOOTREPLY)
	{
		int i = 0;
		for(i=0;i<4;i++)
		{
			p_ip->local_address[i] = dhcp_fields.yiaddr[i];
		}
		
		//skip sname and file
		udp_read(2, p_decal, poubelle, 192);
		
		//read options
		short buf;
		while(udp_availlable(2));
		{
			udp_read(2, p_decal, &buf, 1);
			switch(buf)
			{
				case PAD_OPTION:
					break;
				case SUBNET_MASK:
					udp_read(2, p_decal, &opt_len, 1); //read the length (useless, we know it)
					udp_read(2, p_decal, p_ip->subnet_mask, 4);
					break;
				case ROUTERS_ON_SUBNET:
					udp_read(2, p_decal, &opt_len, 1);
					udp_read(2, p_decal, p_ip->gateway, 4);
					udp_read(2, p_decal, poubelle, opt_len-4); //don't keep the rest of the option
					break;
				case DNS:
					udp_read(2, p_decal, &opt_len, 1);
					udp_read(2, p_decal, p_ip->dns_server_address, 4);
					udp_read(2, p_decal, poubelle, opt_len-4); //don't keep the rest of the option
					break;
				case HOST_NAME:
					udp_read(2, p_decal, &opt_len, 1);
					udp_read(2, p_decal, poubelle, opt_len); //don't keep it
					break;
				case DOMAIN_NAME:
					udp_read(2, p_decal, &opt_len, 1);
					udp_read(2, p_decal, poubelle, opt_len); //don't keep it
					break;
				default:
					udp_read(2, p_decal, &opt_len, 1);
					udp_read(2, p_decal, poubelle, opt_len); //don't keep it
					break;
			}
		}
	}
}

void dhcp_hex_to_ascii(char* buf, short num)
{
	char *str = &buf[1];
	buf[0]='0';
	do
	{
		unsigned long m = num;
		num /= 16;
		char c = m - 16 * num;
		*str-- = c < 10 ? c + '0' : c + 'A' - 10;
	} while(num);
}
