
void udp_init(short* ip, int src_port, int dst_port, short socket);
void udp_begin_packet(short* ip_address, int port);
void udp_end_packet();
void udp_stop(short socket);
short udp_availlable(short socket);
void udp_write_byte(short data, short socket, int* p_offset);
void udp_write_str(char* data, short socket);
void udp_send(short socket);
int udp_parse(short socket, short* src_ip, int* src_port, int* p_offset);
void udp_read(short socket, int* p_offset, short* buffer, short len);
void udp_recv(short socket);
