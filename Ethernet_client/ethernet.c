#include "ethernet.h"
#include "w5500.h"

int server_port[MAX_SOCK_NUM] = {0};

int ethernet_begin()
{
	w5500_init(CS_PIN);

	//adresse ip
	w5500_write(0x0F, 0x04, 192);
	w5500_write(0x10, 0x04, 168);
	w5500_write(0x11, 0x04, 1);
	w5500_write(0x12, 0x04, 177);

	//lire l adresse mac
	short mac_address[4];
	mac_address[0] = w5500_read(0x01, 0x00);
	mac_address[1] = w5500_read(0x02, 0x00);
	mac_address[2] = w5500_read(0x03, 0x00);
	mac_address[3] = w5500_read(0x04, 0x00);
}
