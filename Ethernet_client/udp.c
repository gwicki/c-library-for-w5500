#include "udp.h"
#include "socket.h"
#include "w5500.h"
#include "string.h"
#include "serial.h"

#define CS_PIN 10

#define SOCK_OPEN 0x01
#define SOCK_CONNECT 0x04
#define SOCK_CLOSE 0x10
#define SOCK_ESTABLISHED 0x17
#define SOCK_SEND 0x20
#define SOCK_RECV 0x40

#define UDP 0x02

void udp_init(short* ip, int src_port, int dst_port, short socket)
{
	socket_init(socket, UDP, src_port, 0);

	//adresse ip du client
	w5500_write(0x0F, 0x04, 192);
	w5500_write(0x10, 0x04, 168);
	w5500_write(0x11, 0x04, 1);
	w5500_write(0x12, 0x04, 177);

	//adresse ip du serveur
	w5500_write(0x0C, 0x2C, ip[0]);
	w5500_write(0x0D, 0x2C, ip[1]);
	w5500_write(0x0E, 0x2C, ip[2]);
	w5500_write(0x0F, 0x2C, ip[3]);
}

/*void udp_begin_packet(short* ip_address, int port)
{
	socket_start_udp(0, ip_address, port);
}*/

/*void udp_end_packet()
{
	socket_send(0);
}*/

void udp_stop(short socket)
{
	socket_close(socket);
}

short udp_availlable(short socket)
{
	return w5500_read(0x0027, (socket<<5)+0x08) || w5500_read(0x0026, (socket<<5)+0x08);
}

void udp_write_byte(short data, short socket, int* p_offset)
{
	int wptr = 0;
	//wptr = w5500_read(0x0024, (socket<<5)+0x08);
	//wptr = wptr << 8;
	//wptr |= w5500_read(0x0025, (socket<<5)+0x08);
	
	wptr = *p_offset;
	int essais = 0xF03F;
	/*serial_write('w');
	serial_write(':');
	serial_write_bin((short)((*p_offset) >> 8));
	serial_write_bin((short)((*p_offset) & 0x00FF));
	serial_write(' ');*/
	
	/*int rptr = 0;
	rptr = w5500_read(0x0022, (socket<<5)+0x08);
	rptr = wptr << 8;
	rptr |= w5500_read(0x0023, (socket<<5)+0x08);*/
	
	//serial_write_bin(data);
	//serial_write(' ');
	/*serial_write_bin(w5500_read(wptr, (socket<<5)+0x10));
	serial_write('\n');*/

	//rptr++;
	//w5500_writeSN16(socket, 0x0022, wptr); //deplacer le read pointer

	/*serial_write_bin(w5500_read(0x0024, (socket<<5)+0x08));
	serial_write_bin(w5500_read(0x0025, (socket<<5)+0x08));
	serial_write(',');
	serial_write_bin(w5500_read(0x0028, (socket<<5)+0x08));
	serial_write_bin(w5500_read(0x0029, (socket<<5)+0x08));
	serial_write(' ');*/
	w5500_write(wptr, (socket<<5)+0x14, data);
	wptr++;
	/*serial_write_bin(w5500_read(0x0024, (socket<<5)+0x08));
	serial_write_bin(w5500_read(0x0025, (socket<<5)+0x08));
	serial_write(',');
	serial_write_bin(w5500_read(0x0028, (socket<<5)+0x08));
	serial_write_bin(w5500_read(0x0029, (socket<<5)+0x08));
	serial_write(' ');*/
	(*p_offset) = (*p_offset) + 1;
	essais++;
	
	/*serial_write_bin((short)((*p_offset) >> 8));
	serial_write_bin((short)((*p_offset) & 0x00FF));
	serial_write('\n');*/
	
	w5500_writeSN16(socket, 0x0024, wptr); //deplacer le write pointer
	
	/*serial_write_bin(w5500_read(0x0024, (socket<<5)+0x08));
	serial_write_bin(w5500_read(0x0025, (socket<<5)+0x08));
	serial_write(',');
	serial_write_bin(w5500_read(0x0028, (socket<<5)+0x08));
	serial_write_bin(w5500_read(0x0029, (socket<<5)+0x08));
	serial_write('\n');*/
}

void udp_write_str(char* data, short socket)
{
	int wptr = 0;
	wptr = w5500_read(0x0024, (socket<<5)+0x08);
	wptr = wptr << 8;
	wptr |= w5500_read(0x0025, (socket<<5)+0x08);

	w5500_write_string(wptr, (socket<<5)+0x14, data, strlen(data));
	wptr += strlen(data);
	w5500_writeSN16(socket, 0x0024, wptr); //deplacer le write pointer
}

void udp_send(short socket)
{
	/*serial_write_bin(w5500_read(0x0024, (socket<<5)+0x08));
	serial_write_bin(w5500_read(0x0025, (socket<<5)+0x08));
	serial_write(',');
	serial_write_bin(w5500_read(0x0028, (socket<<5)+0x08));
	serial_write_bin(w5500_read(0x0029, (socket<<5)+0x08));
	serial_write(' ');*/
	w5500_cmd_sock(socket, SOCK_SEND); //envoyer
	/*serial_write_bin(w5500_read(0x0024, (socket<<5)+0x08));
	serial_write_bin(w5500_read(0x0025, (socket<<5)+0x08));
	serial_write(',');
	serial_write_bin(w5500_read(0x0028, (socket<<5)+0x08));
	serial_write_bin(w5500_read(0x0029, (socket<<5)+0x08));
	serial_write('\n');*/
}

//fonction qui lit le pseudo header ip de udp
int udp_parse(short socket, short* src_ip, int* src_port, int* p_offset)
{
	int rptr = 0;
	rptr = *p_offset;
	
	short tmp_buffer[8];
	
	int ret = 0;
	
	/*serial_write('a');
	serial_write_bin(rptr);
	serial_write(' ');*/
	w5500_read_str(rptr, (socket<<5)+0x018, tmp_buffer, 8);
	(*p_offset)+=8;
	/*serial_write_bin(rptr);
	serial_write(' ');*/
	//udp_recv(socket);
	/*serial_write_bin(rptr);
	serial_write('\n');*/
	
	src_ip = tmp_buffer;
	/*serial_write_bin(src_ip[0]);
	serial_write(' ');
	serial_write_bin(src_ip[1]);
	serial_write(' ');
	serial_write_bin(src_ip[2]);
	serial_write(' ');
	serial_write_bin(src_ip[3]);
	serial_write('\n');*/
	*src_port = (tmp_buffer[4] << 8 ) + tmp_buffer[5];
	/*serial_write_bin(tmp_buffer[4]);
	serial_write_bin(tmp_buffer[5]);
	serial_write('\n');*/
	
	int remain = 0;
	remain = (tmp_buffer[6] << 8 ) + tmp_buffer[7];
	
	/*serial_write_bin(tmp_buffer[6]);
	serial_write_bin(tmp_buffer[7]);
	serial_write('\n');*/
	
	return remain;
}

void udp_read(short socket, int* p_offset, short* buffer, short len)
{
	//char c;

	int rptr = 0;
	rptr = *p_offset;
	
	/*rptr = w5500_read(0x0028, (socket<<5)+0x08);
	rptr = (rptr) << 8;
	rptr |= w5500_read(0x0029, (socket<<5)+0x08);*/
	
	/*serial_write('r');
	serial_write(':');
	serial_write_bin((short)(rptr >> 8));
	serial_write_bin((short)(rptr & 0x00FF));
	serial_write(' ');*/
	
	//c = w5500_read_str(rptr, (socket<<5)+0x18, buffer, len);
	w5500_read_str(rptr, (socket<<5)+0x18, buffer, len);
	(*p_offset)+=len;
	
	//udp_recv(socket);
	
	/*serial_write_bin((short)(rptr >> 8));
	serial_write_bin((short)(rptr & 0x00FF));
	serial_write(' ');*/
	
	//(*p_offset) = (*p_offset) + 1;
	
	//rptr ++;
	
	/*serial_write_bin((short)(rptr >> 8));
	serial_write_bin((short)(rptr & 0x00FF));
	serial_write('\n');*/
	
	//w5500_writeSN16(socket, 0x0028, rptr);

	//return c;
}

void udp_recv(short socket)
{
	w5500_cmd_sock(socket, SOCK_RECV);
}
