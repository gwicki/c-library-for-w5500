#include "dns.h"
#include "udp.h"
#include "serial.h"

#include <string.h>

#define DNS_HEADER_SIZE 12
#define ASCII_0 48
#define TTL_SIZE 4
#define RECURSION_DESIRED_FLAG 0x01
#define TYPE_A 0x01
#define CLASS_IN 0x01
#define TTL 3600
#define LABEL_COMPRESSION_MASK 0xC0

/*short dns_extract_bytes(char* ip_address, short* ip_bytes)
{
	short i,j;
	for(i=0;i<4;i++)
	{
		c = ip_address[4*i];
		d = ip_address[4*i+1];
		u = ip_address[4*i+2];

		ip_bytes[i] = 100*c + 10*d + u;

		if(i!=3 && ip_address[4*i+3] != '.')
		{
			return 0;
		}
	}
}*/

/* Le nom fourni dans les RR doit etre sous la forme 3www10duckduckgo3com0
 * avec les nombres en binaire.
 * Cette fonction le met dans cette forme et l envoie.*/

void dns_format_name(char* domain_name, int* p_offset)
{
	const char *start = domain_name;
	const char *end = start;
	int len = 0;
	// Run through the name being requested
	int compteur = 0;
	while(*end)
	{
		// Find out how long this section of the name is
		end = start;
		while(*end && (*end != '.') && (compteur < strlen(domain_name)) )
		{
			end++;
			compteur++;
		}

		if(end-start > 0)
		{
			// Write out the size of this section
			len = end-start;
			
			//il faut envoyer la longueur en carateres ascii
			//on choisit de limiter la longueur a 9999 caracteres
			int i;
			
			udp_write_byte(len, 1, p_offset);
			
			// And then write out the section
			for(i=0;i<end-start;i++)
			{
				udp_write_byte(*(start+i), 1, p_offset);
			}
		}
		start = end+1;
	}

	// On ajoute 0 a la fin du nom de domaine
	udp_write_byte(0, 1, p_offset);
}

short dns_write_request(char* domain_name)
{
	//header begin
	serial_write(domain_name[0]);

	int offset = 0;
	int* p_offset = &offset;
	//*p_offset = 0;

	/*serial_write('a');
	serial_write(':');
	serial_write_bin((short)((*p_offset) >> 8));
	serial_write_bin((short)((*p_offset) & 0x00FF));
	serial_write('\n');*/

	//write ID
	short request_id[2] = {0};
	udp_write_byte(request_id[0], 1, p_offset);
	udp_write_byte(request_id[1], 1, p_offset);

	//write flags
	udp_write_byte(RECURSION_DESIRED_FLAG, 1, p_offset); //other flags at 0
	udp_write_byte(0, 1, p_offset);

	//number of questions
	udp_write_byte(0, 1, p_offset);
	udp_write_byte(1, 1, p_offset);

	//number of answers
	udp_write_byte(0, 1, p_offset);
	udp_write_byte(0, 1, p_offset);

	//number of authority
	udp_write_byte(0, 1, p_offset);
	udp_write_byte(0, 1, p_offset);

	//number of additionnal
	udp_write_byte(0, 1, p_offset);
	udp_write_byte(0, 1, p_offset);

	//header completed

	//RR begin
	serial_write(domain_name[0]);
	dns_format_name(domain_name, p_offset);
	
	//Type
	udp_write_byte(0, 1, p_offset);
	udp_write_byte(TYPE_A, 1, p_offset);
	
	//Classe
	udp_write_byte(0, 1, p_offset);
	udp_write_byte(CLASS_IN, 1, p_offset);
	
	//Time to live mis a 4095 secondes
	udp_write_byte(0, 1, p_offset);
	udp_write_byte(0, 1, p_offset);
	udp_write_byte(0x0F, 1, p_offset);
	udp_write_byte(0xFF, 1, p_offset);
	
	//longueur
	udp_write_byte(0, 1, p_offset);
	udp_write_byte(0, 1, p_offset);
	
	//RR completed
	
	udp_send(1); //envoyer la requete
}

short dns_read_response(short* ip_address)
{
	short header[DNS_HEADER_SIZE];

	int decal = 0;
	int* p_decal = &decal;
	
	short* src_ip = NULL;
	int* src_port = NULL;
	udp_parse(1, src_ip, src_port, p_decal);

	short i,j;
	udp_read(1, p_decal, header, DNS_HEADER_SIZE);

	//controler qu il y a une reponse dans le paquet
	int nbr_answers = 256*header[6] + header[7]; //le nombre de reponses est contenu dans les octets 6 et 7 du header
	if(nbr_answers == 0)
	{
		serial_write('z');
		serial_write('\n');
		return 0;
	}

	//skip questions
	int nbr_questions = 256*header[4] + header[5];
	
	short poubelle = 0;
	
	for(i=0;i<nbr_questions;i++)
	{
		//skip the name
		short length;
		do
		{
			udp_read(1, p_decal, &length, 1); //lire la longueur de la section de nom
			
			if(length > 0)
			{
				while(length--) //post incrementation donc retourne -1 si passe par la boucle
				{
					udp_read(1, p_decal, &poubelle, 1); //on ne garde pas les octets correspondant au nom
				}
			}
		}while(length);

		//skip the type and the class
		for(j=0;j<4;j++)
        {
			udp_read(1, p_decal, &poubelle, 1); //on ne garde pas les valeurs
        }
	}

	//read answer
	for(i=0;i<nbr_answers;i++)
	{
		//skip the name
		short length;
		do
		{
			udp_read(1, p_decal, &length, 1); //lire la longueur de la section de nom
			
			if((length & LABEL_COMPRESSION_MASK) == 0)
			{
				if(length > 0)
				{
					while(length--) //post incrementation donc retourne -1 si passe par la boucle
					{
						udp_read(1, p_decal, &poubelle, 1); //on ne garde pas les octets correspondant au nom
					}
				}
			}
			else //pointeur vers un autre endroit dans le message
			{
				udp_read(1, p_decal, &poubelle, 1); //on ne garde pas les octets correspondant au nom
				length = 0;
			}
		}while(length);

		//read the type and the class
		int answer_type;
		short answer_type_1 = 0;
		short answer_type_2 = 0;
		
		udp_read(1, p_decal, &answer_type_1, 1);
		udp_read(1, p_decal, &answer_type_2, 1);
		
		answer_type = answer_type_1;
		answer_type = answer_type << 8;
		answer_type = answer_type | answer_type_2;

		serial_write('c');
		int answer_class;
		short answer_class_1 = 0;
		short answer_class_2 = 0;
		
		udp_read(1, p_decal, &answer_class_1, 1);
		
		udp_read(1, p_decal, &answer_class_2, 1);
		
		answer_class = answer_class_1;
		answer_class = answer_class << 8;
		answer_class = answer_class | answer_class_2;

		//skip TTL
		for(j=0;j<TTL_SIZE;j++)
        {
            udp_read(1, p_decal, &poubelle, 1); //on ne garde pas les valeurs
        }

		//read the length of the datas
		short data_length = 0;
		short data_length_1 = 0;
		short data_length_2 = 0;
		
		udp_read(1, p_decal, &data_length_1, 1);
		udp_read(1, p_decal, &data_length_2, 1);
		
		data_length = data_length_1;
		data_length = data_length << 8;
		data_length = data_length | data_length_2;
		
		if(answer_type == TYPE_A && answer_class == CLASS_IN)
		{
			//read ip
			udp_read(1, p_decal, ip_address, 4);
		}
		else //that is not the answer with the IP
		{
			for(j=0;j<data_length;j++)
				udp_read(1, p_decal, &poubelle, 1);
		}
	}
	return 1;
}
