//#define F_CPU 16000000UL

#include <avr/io.h>
#include <util/delay.h>
#include "client.h"
#include "udp.h"
#include "serial.h"
#include "w5500.h"
#include "dns.h"

//#define NULL 0
#define SOCK_RECV 0x40
#define SOCK_LISTEN 0x02
#define SOCK_ESTABLISHED 0x17
#define SOCK_RECV 0x40
#define SOCK_DISCON 0x08
#define SOCK_OPEN 0x01

#define CS_PIN 10

typedef struct Ptr PTR;

short server_ip[4] = {192,168,1,122};

void main(void)
{
	w5500_init(CS_PIN);
	
	serial_initialize();
	serial_write('s');
	serial_write('\n');
	
	//requete dns
/*	short dns_server_ip[4] = {192,168,1,122}; //IP address of the DNS server
	udp_init(dns_server_ip, 53, 53, 1); //init of udp communication used for DNS
	serial_write('1');
	serial_write('\n');
	
	const char* url = "wicki-disler.ddns.net";
	dns_write_request(url); //write the request to the DNS server
	serial_write('2');
	serial_write('\n');
	dns_read_response(server_ip); //read the answer from the DNS server
	serial_write('3');
	serial_write('\n');
	
	//IP address of the server
	serial_write_bin(server_ip[0]);
	serial_write('\n');
	serial_write_bin(server_ip[1]);
	serial_write('\n');
	serial_write_bin(server_ip[2]);
	serial_write('\n');
	serial_write_bin(server_ip[3]);
	serial_write('\n');
	*/
	
	client_begin(80, 0);
	
	_delay_ms(1000);
	/*client_connect(0);
	while(!client_connected(0))
	{
		
	}*/
	
	serial_write('c');
	serial_write('\n');

	//connect
	client_connect(server_ip, 80, 0);
	
	//when it arrives to SOCK_ESTABLISHED the connection is ok
	serial_write('q');
	while(w5500_read(0x0003, 0x08) != SOCK_ESTABLISHED)
	{
		serial_write_bin(w5500_read(0x0003, 0x08));
		serial_write('\n');
	}
	
	serial_write('a');
	
	//send the http request
	client_write("GET / HTTP/1.1\r\n", 0);
	client_write("Host: voyage.wicki.ddns.net\r\n", 0);
	client_write("Accept: */*\r\n", 0);
	//client_write("Connection: close\r\n", 0);
	client_write("\r\n", 0);
	
	serial_write('b');

	int wptr = 0; //write pointer
	int rptr = 0;

	char c_ppprec = 0;
	char c_pprec = 0;
	char c_prec = 0;
	char c = 0;

	while(1)
	{
		/*if(client_availlable(0))
		{
			c = client_read(0);
			serial_write_bin(c);
			serial_write('\n');
		}*/
		
		//listen
		//w5500_cmd_sock(0, SOCK_LISTEN);
		
		//read http answer
		
		while(w5500_read(0x0027, 0x08) || w5500_read(0x0026, 0x08))
		{
			c = client_read(0);
			serial_write(c);

			if(c == '\n' && c_prec == '\r' && c_pprec == '\n' && c_ppprec == '\r')
			{
				/*client_write("HTTP/1.0 200 OK\r\n", 0);
				client_write("Content-Type: text/html\r\n", 0);
				client_write("Connection: close\r\n", 0);
				client_write("Refresh: 5\r\n", 0);
				client_write("\r\n", 0);
				client_write("<!DOCTYPE HTML>\r\n", 0);
				client_write("<html>\r\n", 0);
				client_write("Bonjour\r\n", 0);
				client_write("</html>\r\n", 0);*/

				/*w5500_cmd_sock(0, SOCK_DISCON);
				_delay_ms(1);
				w5500_cmd_sock(0, SOCK_OPEN);*/
			}
			c_ppprec = c_pprec;
			c_pprec = c_prec;
			c_prec = c;
		}
		
		//read html
		while(w5500_read(0x0027, 0x08) || w5500_read(0x0026, 0x08))
		{
			c = client_read(0);
			serial_write(c);
		}
		
		w5500_cmd_sock(0, SOCK_DISCON);
		_delay_ms(1);
		w5500_cmd_sock(0, SOCK_OPEN);
		
	}
}
