#include <string.h>
#include "client.h"
#include "socket.h"
#include "w5500.h"
#include "dhcp.h"
#include "registers.h"
#include "serial.h"

#define CS_PIN 10

#define SOCK_OPEN 0x01
#define SOCK_CONNECT 0x04
#define SOCK_CLOSE 0x10
#define SOCK_ESTABLISHED 0x17
#define SOCK_SEND 0x20
#define SOCK_RECV 0x40

#define TCP 0x01

struct Client
{
	int port;
	short socket;
	CLIENT* suivant;
};

void client_begin(int src_port, short socket)
{
	w5500_init(CS_PIN);

	short mac_address[6] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
	dhcp_request_lease(mac_address);

	//adresse ip du client
	w5500_write(0x0F, 0x04, 192);
	w5500_write(0x10, 0x04, 168);
	w5500_write(0x11, 0x04, 1);
	w5500_write(0x12, 0x04, 177);
	
	socket_init(socket, TCP, src_port, 0);
}

short client_connected(short socket)
{
	return w5500_read(0x0003, (socket<<5)+0x08) == SOCK_ESTABLISHED;
}

short client_availlable(short socket)
{
	return w5500_read(0x0027, (socket<<5)+0x08) || w5500_read(0x0026, (socket<<5)+0x08);
}

void client_connect(short* dst_ip, int dst_port, short socket)
{
	//lire le status register du socket 0
	short statut = w5500_read(0x0003, (socket<<5)+0x08);
	serial_write_bin(statut);
	serial_write('\n');
	
	//adresse ip du serveur
	w5500_write(0x0C, 0x0C, dst_ip[0]);
	w5500_write(0x0D, 0x0C, dst_ip[1]);
	w5500_write(0x0E, 0x0C, dst_ip[2]);
	w5500_write(0x0F, 0x0C, dst_ip[3]);
	
	//specifier le port destination
	w5500_writeSN16(socket, 0x0010, dst_port);
	
	w5500_cmd_sock(socket, SOCK_CONNECT);
}

short client_read(short socket)
{
	char c;

	int rptr = 0;
	rptr = w5500_read(0x0028, (socket<<5)+0x08);
	rptr = rptr << 8;
	rptr |= w5500_read(0x0029, (socket<<5)+0x08);
	c = w5500_read(rptr, 0x18);

	rptr ++;
	w5500_writeSN16(socket, 0x0028, rptr);
	w5500_cmd_sock(socket, SOCK_RECV);

	return c;
}

void client_write(char* data, short socket)
{
	int wptr = 0;
	wptr = w5500_read(0x0024, (socket<<5)+0x08);
	wptr = wptr << 8;
	wptr |= w5500_read(0x0025, (socket<<5)+0x08);
	
	serial_write_bin(wptr);
	serial_write(' ');
	serial_write_bin(w5500_read(0x0024, (socket<<5)+0x08));
	serial_write_bin(w5500_read(0x0025, (socket<<5)+0x08));
	serial_write(' ');
	serial_write_bin(w5500_read(0x0022, (socket<<5)+0x08));
	serial_write_bin(w5500_read(0x0023, (socket<<5)+0x08));
	serial_write('\n');

	w5500_write_string(wptr, (socket<<5)+0x14, data, strlen(data));
	wptr += strlen(data);
	
	serial_write_bin(wptr);
	serial_write(' ');
	serial_write_bin(w5500_read(0x0024, (socket<<5)+0x08));
	serial_write_bin(w5500_read(0x0025, (socket<<5)+0x08));
	serial_write(' ');
	serial_write_bin(w5500_read(0x0022, (socket<<5)+0x08));
	serial_write_bin(w5500_read(0x0023, (socket<<5)+0x08));
	serial_write('\n');
	
	w5500_writeSN16(socket, 0x0024, wptr); //deplacer le write pointer
	
	serial_write_bin(wptr);
	serial_write(' ');
	serial_write_bin(w5500_read(0x0024, (socket<<5)+0x08));
	serial_write_bin(w5500_read(0x0025, (socket<<5)+0x08));
	serial_write(' ');
	serial_write_bin(w5500_read(0x0022, (socket<<5)+0x08));
	serial_write_bin(w5500_read(0x0023, (socket<<5)+0x08));
	serial_write('\n');
	
	w5500_cmd_sock(socket, SOCK_SEND); //envoyer
	
	serial_write_bin(wptr);
	serial_write(' ');
	serial_write_bin(w5500_read(0x0024, (socket<<5)+0x08));
	serial_write_bin(w5500_read(0x0025, (socket<<5)+0x08));
	serial_write(' ');
	serial_write_bin(w5500_read(0x0022, (socket<<5)+0x08));
	serial_write_bin(w5500_read(0x0023, (socket<<5)+0x08));
	serial_write('\n');
	serial_write('\n');
}
/*short client_status(short sock)
{
	if (sock == MAX_SOCK_NUM)
	{
		return SnSR_CLOSED;
	}
	return w5500_readSN8(sock, 0x0003);
}*/
