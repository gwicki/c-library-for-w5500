
typedef struct Client CLIENT;

void client_begin(int src_port, short socket);
short client_connected(short socket);
short client_availlable(short socket);
void client_connect(short* dst_ip, int dst_port, short socket);
short client_read(short socket);
void client_write(char* data, short socket);
//short client_status(short sock);
