#define MAX_SOCK_NUM 8

void server_begin(int port, short socket);
short server_connected(short socket);
short server_availlable(short socket);
void server_listen(short socket);
short server_read(short socket);
void server_write(char* data, short socket);
void server_disconnect(short socket);
void server_open(short socket);
