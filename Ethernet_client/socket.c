#include "socket.h"
#include "w5500.h"
#include "registers.h"
#include "serial.h"

#define SOCK_OPEN 0x01
#define SOCK_LISTEN 0x02
#define SOCK_CONNECT 0x04
#define SOCK_DISCON 0x08
#define SOCK_CLOSE 0x10

//uint16_t local_port;

short socket_init(short sock, short protocol, int src_port, short flag)
{
	if(protocol == SNMR_TCP || protocol == SNMR_UDP || protocol == SNMR_IPRAW || protocol == SNMR_MACRAW || protocol == SNMR_PPPOE)
	{
		socket_close(sock);
		
		//set the socket Mode Register
		w5500_writeSN8(sock, 0x0000, protocol | flag);
		
		//source port
		w5500_writeSN16(sock, 0x0004, src_port);
		
		/*w5500_writeSn8(sock, 0x0000, protocol | flag);
		if(port)
		{
			w5500_writeSn16(sock, 0x0004, port);
		}
		else
		{
			local_port++; //?
			w5500_writeSn16(sock, 0x0004, local_port); //?
		}*/
		
		//initialize the socket and open it according to the selected protocol
		w5500_cmd_sock(sock, SOCK_OPEN);
		return 1;
	}
	return 0;
}

void socket_close(short sock)
{
	w5500_cmd_sock(sock, SOCK_CLOSE);
	//w5500_writeSn8(sock, 0x0002, 0xFF);
}

//put the socket in "LISTEN" mode in order to wait for a SYN packet from a client
short socket_listen(short sock)
{
	if(w5500_readSN8(sock, 0x0003) != SNSR_INIT)
	{
		return 0;
	}
	w5500_cmd_sock(sock, SOCK_LISTEN);
	return 1;
}

short socket_start_udp(short sock, short* dst_ip, int dst_port)
{
	//set destination ip
	w5500_write(0x0C, 0x0C, dst_ip[0]);
	w5500_write(0x0D, 0x0C, dst_ip[1]);
	w5500_write(0x0E, 0x0C, dst_ip[2]);
	w5500_write(0x0F, 0x0C, dst_ip[3]);

	//set destination port
	w5500_writeSN16(sock, 0x0010, dst_port);
}
