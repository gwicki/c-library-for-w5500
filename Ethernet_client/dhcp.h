
typedef struct Ip IP;
typedef struct Dhcp_fields DHCP_FIELDS;

int dhcp_begin(short* mac_address, unsigned long timeout, unsigned long respose_timeout);
void dhcp_request_lease(short* mac_address);
void dhcp_send_request(unsigned long dhcp_transaction_id, short message_type, int seconds_elapsed, IP* ip);
void dhcp_read_response(IP* p_ip);
void dhcp_hex_to_ascii(char* buf, short num);
