#include <avr/io.h>
#include <util/delay.h>
#include "w5500.h"
#include "serial.h"

#define SOCKET 0
#define CS_PIN 10
#define SOCK_OPEN 0x01
#define SOCK_CLOSE 0x10
#define SOCK_LISTEN 0x02
#define SOCK_SEND 0x20
#define SOCK_RECV 0x40
#define SOCK_DISCON 0x08

#define SOCK_ESTABLISHED 0x17

#define TCP 0x01

void main(void)
{
	w5500_init(CS_PIN);

	//adresse ip
	w5500_write(0x0F, 0x04, 192);
	w5500_write(0x10, 0x04, 168);
	w5500_write(0x11, 0x04, 1);
	w5500_write(0x12, 0x04, 177);

	//close the socket
	w5500_cmd_sock(SOCKET, SOCK_CLOSE);
	w5500_writeSN8(SOCKET, 0x0002, 0xFF);

	//specifier le protocole TCP
	w5500_writeSN8(SOCKET, 0x0000, TCP);

	const int port = 80;
	//specifier le port http
	w5500_writeSN16(SOCKET, 0x0004, port);

	int wptr = 0; //write pointer
	int rptr = 0;
	char c_ppprec = 0;
	char c_pprec = 0;
	char c_prec = 0;
	char c = 0;

	//open the socket
	w5500_cmd_sock(SOCKET, SOCK_OPEN);

	while(1)
	{
		//listen
		w5500_cmd_sock(SOCKET, SOCK_LISTEN);

		//il faut arriver a l'etat sock_established
		while(w5500_read(0x0003, 0x08) != SOCK_ESTABLISHED)
		{
			
		}

		if(w5500_read(0x0027, 0x08) || w5500_read(0x0026, 0x08))
		{
			rptr = w5500_read(0x0028, 0x08);
			rptr = rptr << 8;
			rptr |= w5500_read(0x0029, 0x08);

			c = w5500_read(rptr, 0x18);
			rptr ++;
			w5500_writeSN16(SOCKET, 0x0028, rptr);
			w5500_cmd_sock(SOCKET, SOCK_RECV);
	
			if(c == '\n' && c_prec == '\r' && c_pprec == '\n' && c_ppprec == '\r')
			{
				//lire le write pointer
				wptr = w5500_read(0x0024, 0x08);
				wptr = wptr << 8;
				wptr |= w5500_read(0x0025, 0x08);

				w5500_write_string(wptr, 0x14, "HTTP/1.0 200 OK", 15);
				wptr += 15;
				w5500_writeSN16(SOCKET, 0x0024, wptr); //deplacer le write pointer
				w5500_cmd_sock(SOCKET, SOCK_SEND); //envoyer
				w5500_write(wptr, 0x14, '\r');
				wptr ++;
				w5500_write(wptr, 0x14, '\n');
				wptr ++;
				w5500_writeSN16(SOCKET, 0x0024, wptr); //deplacer le write pointer
				w5500_cmd_sock(SOCKET, SOCK_SEND); //envoyer
				w5500_write_string(wptr, 0x14, "Content-Type: text/html", 23);
				wptr += 23;
				w5500_writeSN16(SOCKET, 0x0024, wptr); //deplacer le write pointer
				w5500_cmd_sock(SOCKET, SOCK_SEND); //envoyer
				w5500_write(wptr, 0x14, '\r');
				wptr ++;
				w5500_write(wptr, 0x14, '\n');
				wptr ++;
				w5500_writeSN16(SOCKET, 0x0024, wptr); //deplacer le write pointer
				w5500_cmd_sock(SOCKET, SOCK_SEND); //envoyer
				w5500_write_string(wptr, 0x14, "Connection: close", 17);
				wptr += 17;
				w5500_writeSN16(SOCKET, 0x0024, wptr); //deplacer le write pointer
				w5500_cmd_sock(SOCKET, SOCK_SEND); //envoyer
				w5500_write(wptr, 0x14, '\r');
				wptr ++;
				w5500_write(wptr, 0x14, '\n');
				wptr ++;
				w5500_writeSN16(SOCKET, 0x0024, wptr); //deplacer le write pointer
				w5500_cmd_sock(SOCKET, SOCK_SEND); //envoyer
				w5500_write_string(wptr, 0x14, "Refresh: 5", 10);
				wptr += 10;
				w5500_writeSN16(SOCKET, 0x0024, wptr); //deplacer le write pointer
				w5500_cmd_sock(SOCKET, SOCK_SEND); //envoyer
				w5500_write(wptr, 0x14, '\r');
				wptr ++;
				w5500_write(wptr, 0x14, '\n');
				wptr ++;
				w5500_writeSN16(SOCKET, 0x0024, wptr); //deplacer le write pointer
				w5500_cmd_sock(SOCKET, SOCK_SEND); //envoyer
				w5500_write(wptr, 0x14, '\r');
				wptr ++;
				w5500_write(wptr, 0x14, '\n');
				wptr ++;
				w5500_writeSN16(SOCKET, 0x0024, wptr); //deplacer le write pointer
				w5500_cmd_sock(SOCKET, SOCK_SEND); //envoyer
				w5500_write_string(wptr, 0x14, "<!DOCTYPE HTML>", 15);
				wptr += 15;
				w5500_writeSN16(SOCKET, 0x0024, wptr); //deplacer le write pointer
				w5500_cmd_sock(SOCKET, SOCK_SEND); //envoyer
				w5500_write(wptr, 0x14, '\r');
				wptr ++;
				w5500_write(wptr, 0x14, '\n');
				wptr ++;
				w5500_writeSN16(SOCKET, 0x0024, wptr); //deplacer le write pointer
				w5500_cmd_sock(SOCKET, SOCK_SEND); //envoyer
				w5500_write_string(wptr, 0x14, "<html>", 6);
				wptr += 6;
				w5500_writeSN16(SOCKET, 0x0024, wptr); //deplacer le write pointer
				w5500_cmd_sock(SOCKET, SOCK_SEND); //envoyer
				w5500_write(wptr, 0x14, '\r');
				wptr ++;
				w5500_write(wptr, 0x14, '\n');
				wptr ++;
				w5500_writeSN16(SOCKET, 0x0024, wptr); //deplacer le write pointer
				w5500_cmd_sock(SOCKET, SOCK_SEND); //envoyer

				w5500_write_string(wptr, 0x14, "Bonjour", 7);
				wptr += 7;
				w5500_writeSN16(SOCKET, 0x0024, wptr);
				w5500_cmd_sock(SOCKET, SOCK_SEND);

				w5500_write(wptr, 0x14, '\r');
				wptr ++;
				w5500_write(wptr, 0x14, '\n');
				wptr ++;
				w5500_writeSN16(SOCKET, 0x0024, wptr); //deplacer le write pointer
				w5500_cmd_sock(SOCKET, SOCK_SEND); //envoyer

				w5500_write_string(wptr, 0x14, "</html>", 7);
				wptr += 7;
				w5500_writeSN16(SOCKET, 0x0024, wptr); //deplacer le write pointer
				w5500_cmd_sock(SOCKET, SOCK_SEND); //envoyer

				w5500_write(wptr, 0x14, '\r');
				wptr ++;
				w5500_write(wptr, 0x14, '\n');
				wptr ++;
				w5500_writeSN16(SOCKET, 0x0024, wptr); //deplacer le write pointer
				w5500_cmd_sock(SOCKET, SOCK_SEND); //envoyer

				w5500_cmd_sock(SOCKET, SOCK_DISCON);

				_delay_ms(1);
				w5500_cmd_sock(SOCKET, SOCK_OPEN);
			}
			c_ppprec = c_pprec;
			c_pprec = c_prec;
			c_prec = c;
		}
	}
}
