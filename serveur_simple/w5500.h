
#define MAX_SOCK_NUM 8

void w5500_init(short ss_pin);
void w5500_write(int address, short control, short data);
void w5500_write_string(int address, short control, char* data, int len);
short w5500_read(int address, short control);
void w5500_writeSN8(short sock, int address, short data);
void w5500_writeSN16(short sock, int address, int data);
short w5500_readSN8(short sock, int address);
void w5500_cmd_sock(short sock, short command);
